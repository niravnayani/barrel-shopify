export default el => {
  var adjustPosition = () => {
    var height = document.querySelector('.featured__background').clientHeight
    document.querySelector('.grid').style.marginTop = height + 'px'
  }
  window.addEventListener('load', function () {
    adjustPosition()
  })
  window.addEventListener('resize', function () {
    adjustPosition()
  })
}
